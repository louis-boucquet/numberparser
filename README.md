# NumberParser

This is a java library to parse numbers from `java.lang.String`.

## installation

1. clone this repository
1. move to the directory you just cloned
2. run the installer
    * on windows: run `.\install`
    * on unix (mac): run `./install`

## importing

gradle dependency:
```groovy
dependencies {
    compile 'louis:number-parser:1.0'
}
```

## usage

for ease of use import all of the converters
```java
import static louis.numberConverter.NumberConverter.*;
```

use the converters:

```java
binary.convert("10"); // 2
octal.convert("10"); // 8
hexadecimal.convert("10"); // 16
decimal.convert("10"); // 10
```

or give the format as a literal:

```java
parseLiteral("0b10"); // 2
parseLiteral("010"); // 8
parseLiteral("0x10"); // 16
parseLiteral("10"); // 10
```
