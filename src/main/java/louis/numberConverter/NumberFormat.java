package louis.numberConverter;

public class NumberFormat implements DigitConverter {
    private final int base;
    private final DigitConverter digitConvert;

    public NumberFormat(int base, DigitConverter digitConvert) {
        this.base = base;
        this.digitConvert = digitConvert;
    }

    public int getBase() {
        return base;
    }

    @Override
    public int convert(char c) {
        return digitConvert.convert(c);
    }

    public static NumberFormat fromDigits(char[] digits) {
        return new NumberFormat(digits.length, (char c) -> {
            for (int i = 0; i < digits.length; i++) {
                if (digits[i] == c)
                    return i;
            }
            throw new UnknownError();
        });
    }
}
