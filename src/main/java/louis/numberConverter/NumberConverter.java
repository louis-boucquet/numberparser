package louis.numberConverter;

import jdk.nashorn.internal.runtime.regexp.joni.exception.SyntaxException;

import java.util.HashMap;
import java.util.Map;

public class NumberConverter {
    private NumberFormat format;

    public NumberConverter(NumberFormat numberFormat) {
        this.format = numberFormat;
    }

    public NumberConverter(char[] digits) {
        this.format = NumberFormat.fromDigits(digits);
    }

    public int convert(String number) {
        number = new StringBuilder(number)
                .reverse()
                .toString();

        int out = 0;
        int index = 1;

        for (char c : number.toCharArray()) {
            try {
                out += format.convert(c) * index;
                index *= format.getBase();
            } catch (UnknownError e) {
                throw new SyntaxException("Unknown char: " + c);
            }
        }

        return out;
    }

    // STATIC FINAL CONVERTERS

    public static final NumberConverter binary =
            new NumberConverter(new char[]{'0', '1'});

    public static final NumberConverter octal =
            new NumberConverter(new char[]{
                    '0', '1', '2', '3', '4', '5', '6', '7'
            });

    public static final NumberConverter hexadecimal =
            new NumberConverter(new char[]{
                    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
            });

    public static final NumberConverter decimal =
            new NumberConverter(new char[]{
                    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
            });

    // STATIC CONVERT

    public static int parseLiteral(String number) {
        if (number.startsWith("0b"))
            return binary.convert(number.substring(2));
        if (number.startsWith("0x"))
            return hexadecimal.convert(number.substring(2));
        if (number.startsWith("0"))
            return octal.convert(number.substring(1));
        else
            return decimal.convert(number);
    }
}
