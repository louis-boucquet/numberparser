package louis.numberConverter;

public interface DigitConverter {
    int convert(char c) throws UnknownError;
}
