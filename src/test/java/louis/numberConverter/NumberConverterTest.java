package louis.numberConverter;

import org.junit.Test;

import static louis.numberConverter.NumberConverter.*;
import static org.junit.Assert.assertEquals;

public class NumberConverterTest {
    @Test
    public void converterBinary() {
        assertEquals(2, binary.convert("10"));
    }

    @Test
    public void converterOctal() {
        assertEquals(8, octal.convert("10"));
    }

    @Test
    public void converterHexadecimal() {
        assertEquals(16, hexadecimal.convert("10"));
    }

    @Test
    public void converterDecimal() {
        assertEquals(10, decimal.convert("10"));
    }

    @Test
    public void parseLiteralTest() {
        assertEquals(2, parseLiteral("0b10"));
        assertEquals(8, parseLiteral("010"));
        assertEquals(16, parseLiteral("0x10"));
        assertEquals(10, parseLiteral("10"));
    }
}